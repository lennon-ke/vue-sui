import Vue from 'vue'
import app from './main'
import VueRouter from 'vue-router'
import routerConfig from './router'
/* eslint-disable no-new */
Vue.use(VueRouter);

const router = new VueRouter({
  hashbang: true,
  history: false,
  saveScrollPosition: true,
  suppressTransitionError: true
});

routerConfig(router);

router.start(app,'#app');
window.router = router;

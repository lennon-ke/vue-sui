/**
 * Created by dg_lennon on 16/3/14.
 */
import Home from './views/home'
export default function (router) {
  router.map({
    '/home': {
      component: Home
    }
  });
  router.redirect({
    '/': '/home'
  });

  router.beforeEach(function ({to, from, next}) {
    let toPath = to.path;
    let fromPath = from.path;
    console.log('to: ' + toPath + ' from: ' + fromPath);
    if (toPath.replace(/[^/]/g, '').length > 1) {
      router.app.isIndex = false
    }
    router.app.isIndex = true;

    // console.log($.device);
    next()
  });

  router.afterEach(function ({to}) {
    console.log(`成功浏览到: ${to.path}`);
    $.refreshScroller()
  })
}
